@Preview
@CustomDialog
@Component
struct HmConfirm {
  controller: CustomDialogController
  // 两部分 确认内容  按钮
  message: string = "确定要退出登录吗？" // 默认值
  // 接收按钮的类型
  buttonsList: HmConfirmButton[] = [{
    text: "确定",
    fontSize: 14,
    fontColor: $r("app.color.primary")
  }]

  build() {
    Column() {
      // 显示内容
      Row() { // 默认垂直居中
        Text(this.message)
          .fontSize(15)
          .fontColor($r("app.color.text_primary"))
      }
      .justifyContent(FlexAlign.Center)
      .border({
        width: {
          bottom: 0.5
        },
        color: $r("app.color.background_divider")
      })
      .width("100%")
      .height(90)

      // 按钮均分  循环创建
      Row() {
        ForEach(this.buttonsList, (item: HmConfirmButton, index: number) => {
          Text(item.text)
            .height('100%')
            .fontColor(item.fontColor || $r("app.color.text_secondary"))
            .fontSize(item.fontSize || 16)
            .layoutWeight(1)
            .textAlign(TextAlign.Center)
            .border({
              width: {
                right: index !== this.buttonsList.length - 1 ? 0.5 : 0
              },
              color: $r("app.color.text_secondary")
            })
            .onClick(async () => {
              if (item.action) {
                await item.action()
              }
              this.controller.close()
            })
        })
      }
      .width('100%')
      .height(50)
    }
    .borderRadius(12)
    .backgroundColor($r("app.color.white"))
    .width(278)
  }
}

// 专门设置按钮的类型
class HmConfirmButton {
  text: string = "" // 文本
  fontSize: number = 12 // 文本大小
  fontColor: ResourceStr = "" // 字体颜色
  action?: () => void = () => {
  }
}


export { HmConfirm, HmConfirmButton }